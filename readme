# _Project_

_Description: This project turns a low-cost off-the-shelf microprocessor board into an electronic pulse 
time stamping and multiple coincidence detection instrument for single photon detection
and statistical analysis in quantum information experiments.  The included software and firmware
collect and collect and statistically process "click" data in real time, then send the results 
to a PC via USB 2.0 from a board built around a field-programmable gate array (FPGA) chip. 

Users can access data through a set of National Instruments' LabVIEW VIs, 
or directly through an included dynamic-link library (DLL). 

The user can access the collected data on request from the FPGA board, 
and can use this data in custom applications. This is the second iteration of the project; 
the main upgrade is a transfer to USB drivers which work with Windows 7, 32- and 64-bit platforms. 
However a few other improvements and additions have been made also.

The code is open source and free. Users can modify the data collection algorithm to create a custom instrument. 
Because data transfer protocols are already written and debugged, users can significantly decrease 
development time. At the same time, the project requires a minimal investment in hardware. The bulk 
of the cost lies in an inexpensive and commercially available FPGA testing board which requires 
no significant modifications. Setup requires only software installation and wiring and mounting the board, 
which involves basic soldering and hole-punching or drilling sheet metal.

A copy of the manual may be found at the NIST project page at: https://physics.nist.gov/fpga

or the LaTeX source at Sharelatex: https://www.sharelatex.com/project/52d98c8c1008dec6130004e3

## Project Setup

1. The project works on Windows 7 or later Windows PCs and requires a USB 2.0 port for data transfer.
2. Download the source code at https://physics.nist.gov/fpga
or at  https://bitbucket.org/joffreyp/stats 
3. Follow the detailed instructions in the .pdf manual (included in the project download, or available as
LaTeX source at: https://physics.nist.gov/fpga)

4. After the setup described in the manual, try the included Counts or TimeTag LabVIEW codes with their
associated firmwares as described in the manual. Ensure that your computer is connecting properly with the board
and that counts are registered and transferred to the PC.


## Testing

The project does not include automated tests, but you can test the project simply by providing a TTL source
to one or more inputs on the FPGA board, and running the Counts or TimeTag LabVIEW program to see that
clicks are registered and transferred to the PC as expected.


## Troubleshooting & Useful Tools

Read the documentation carefully! 90 % of questions have been answered in the manual!

But feel free to contact us by e-mail. Contact addresses may be found at the project's site:

https://physics.nist.gov/fpga

## Contributing changes

Let us know if you have changes you would like to include in our software by e-mail.
